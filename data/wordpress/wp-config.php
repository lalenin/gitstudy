<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'wordpress' );

/** Database password */
define( 'DB_PASSWORD', 'wordpre5s' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vPOARoCUc)$a6SUI1cjIj#D3)BAG6jOdW08ZbzKU1:tqsD8bD~Am:%U7WWV=4E@R');
define('SECURE_AUTH_KEY',  'CT*0cMjs-ae1i]Uw9+3G/WH@@N}y^YSlSGYlf>,M3;pE~/4oyS:|C+u<o($5/nar');
define('LOGGED_IN_KEY',    'Her-+7 P+qo@nV%h7|z_-ePubMEC#&78-es]ET<4%Jq3Jna{;tig|AqO]$j=-U{1');
define('NONCE_KEY',        'ozg@sb/uOXEilUjjKmPbij8-|2CPV+qI3|#_-|;!Anc,ZwEga*Zyu@3c*YfKzAqc');
define('AUTH_SALT',        'P<V>Qv-@`<hF!}t;SF5nsJJ/+6Ph+%?Bc;t(tWl~[.{W8aG0|x+*ahO+x9;>Erj@');
define('SECURE_AUTH_SALT', '4-`|/,aG2t`:j8-Y9p2tKwgKFO-(r,iTSbnKVq0WhYcxY8TOJPvKIC#r4e:RW/X.');
define('LOGGED_IN_SALT',   'pak~ 5FlBw3+*@vHml|O>lVe |<,Og]jQl$<|Dv.!W(&[HLpy|=6y[+}PnN6tE#)');
define('NONCE_SALT',       'CT F<Bi6iYCB+13=?1 @`.[XMgkQKlp4`+PES{|IA7?o8Fu!tgmiYWS/4uIP#A^r');

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
